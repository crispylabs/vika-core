
"""
MAIN.PY
v0

##### AUTHOR #####
Dean Pierozzi

## CONTRIBUTORS ##
-

## DEPENDENCIES ##
-

#### PROJECT #####
VIKA

## DESCRIPTION ###
Main module

##### NOTES ######
To be seen whether this one will be necessary or not later on


"""

import crawler
import trainer

if __name__ == '__main__':
    data = crawler.data_getter()
    trainer.train(data)

