"""
ENVIRONMENT.PY
v0

##### AUTHOR #####
Dean Pierozzi

## CONTRIBUTORS ##
-

## DEPENDENCIES ##
numpy
pandas

#### PROJECT #####
VIKA

## DESCRIPTION ###
Simulation environment class for bot training and acting

##### NOTES ######
Actor-Critic networks approach

"""

import numpy as np
import pandas as pd

class WalletSimulation