"""
CRAWLER.PY
v0

##### AUTHOR #####
Dean Pierozzi

## CONTRIBUTORS ##
-

## DEPENDENCIES ##
pandas
pandas_datareader

#### PROJECT #####
VIKA

## DESCRIPTION ###
Remote API getter module for retrieving and parsing market data

##### NOTES ######
API exploration phase

"""

import pandas as pd
import pandas_datareader as web
import datetime as dt

def data_getter():
    start = dt.datetime(2018,1,1)
    end = dt.datetime.now()
    
    btc = web.DataReader('BTC-USD', 'yahoo', start, end)
    
    #btc = web.get_data_tiingo('BTCUSDT',key)
    return btc
    


# main case for debug purposes
if __name__ == '__main__':
    data = data_getter()
    print(data)