"""
TRAINER.PY
v0

##### AUTHOR #####
Dean Pierozzi

## CONTRIBUTORS ##
-

## DEPENDENCIES ##
numpy
pandas

#### PROJECT #####
VIKA

## DESCRIPTION ###
Training module using processed input data to produce HDF5s of versionized models

##### NOTES ######
Actor-Critic networks approach

"""

import utils

import numpy as np
import pandas as pd
import datetime as dt
import matplotlib.pyplot as mpl

from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split

# to be parametrized by versioning management
TRAIN_SET_SIZE = .6
VALIDATION_SET_SIZE = .2
TEST_SET_SIZE = .2
SEED = 69
TIME_INTERVAL = 100
FEATURES_SELECTED = ['Close','Volume']


def train(data):
    
    debug = utils.Logger(verbose = True, realtime = True)
    
    btc = pd.DataFrame(data)
    btc = btc[FEATURES_SELECTED]
    
    debug.log("Input data size",len(btc))
    debug.log("Input data features",btc.columns)
    debug.log("Input data head",btc.head())
    
    
    
    # features standardization
    
    scaler_price = MinMaxScaler(feature_range=(0,1))
    scaler_volume = MinMaxScaler(feature_range=(0,1))
    
    close_series = np.array(btc['Close']).reshape(-1,1)
    volume_series = np.array(btc['Volume']).reshape(-1,1)
    
    close_series = scaler_price.fit_transform(close_series)
    volume_series = scaler_volume.fit_transform(volume_series)
    
    debug.log("Scaled price size",close_series.size)
    debug.log("Scaled price shape",close_series.shape)
    debug.log("Scaled price head",close_series[:5])
    
    debug.log("Scaled volume size",volume_series.size)
    debug.log("Scaled volume shape",volume_series.shape)
    debug.log("Scaled volume head",volume_series[:5])
    
    #mpl.plot(close_series)
    #mpl.plot(volume_series)
    
    btc_scaled = []
    for i in range(len(close_series)):
        btc_scaled.append((close_series[i],volume_series[i]))


    
    # time series intervals
    
    dataset = []
    for i in range(len(btc_scaled)-TIME_INTERVAL-1):
        X = btc_scaled[i:(i+TIME_INTERVAL)]
        y = btc_scaled[i + TIME_INTERVAL][0]
        dataset.append((X, y))
    #dataset = np.array(dataset)
    
    debug.log("Dataset size",len(dataset))
    debug.log("Dataset head X length",len(dataset[0][0]))
    debug.log("Dataset head y",dataset[0][1])
    
    
    
    # splits
    
    train_data, ds_rest = train_test_split(dataset,train_size=TRAIN_SET_SIZE,random_state=SEED)
    val_data, test_data = train_test_split(ds_rest,train_size=(VALIDATION_SET_SIZE/(VALIDATION_SET_SIZE+TEST_SET_SIZE)),random_state=SEED)
    
    debug.log("Training data size",len(train_data))
    debug.log("Validation data size",len(val_data))
    debug.log("Test data size",len(test_data))
    
    
    
    X_train, X_val, X_test = [], [], []
    y_train, y_val, y_test = [], [], []
    
    for record in train_data:
        X_train.append(record[0])
        y_train.append(record[1])
        
    for record in val_data:
        X_val.append(record[0])
        y_val.append(record[1])
        
    for record in test_data:
        X_test.append(record[0])
        y_test.append(record[1])
    
    X_train = np.array(X_train)
    X_val = np.array(X_val)
    X_test = np.array(X_test)
    y_train = np.array(y_train)
    y_val = np.array(y_val)
    y_test = np.array(y_test)
    
    debug.log("Training X size",X_train.size)
    debug.log("Training X shape",X_train.shape)
    debug.log("Validation X size",X_val.size)
    debug.log("Validation X shape",X_val.shape)
    debug.log("Testing X size",X_test.size)
    debug.log("Testing X shape",X_test.shape)
    
    debug.log("Training y shape",y_train.shape)
    debug.log("Validation y shape",y_val.shape)
    debug.log("Testing y shape",y_test.shape)
    
    
    
    
    debug.export(filename="training")