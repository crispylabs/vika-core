"""
UTILS.PY
v0

##### AUTHOR #####
Dean Pierozzi

## CONTRIBUTORS ##
-

## DEPENDENCIES ##
-

#### PROJECT #####
VIKA

## DESCRIPTION ###
Utils functions

##### NOTES ######
-

"""

class Logger:
    def __init__(self, verbose = True, realtime = True):
        self.logbook = []
        self.verbose = verbose
        self.realtime = realtime
    
    def log(self, header, text):
        record = ("DEBUG",str(header),str(text))
        if self.verbose:
            self.logbook.append(record)
        if self.realtime:
            print(record)
            
    def error(self, header, text):
        record = ("ERROR",str(header),str(text))
        self.logbook.append(record)
        if self.realtime:
            print(record)
            
    def export(self, filetype='txt', filename='logs'):
        if filetype == 'txt' or filetype == 'text':
            with open((str(filename)+".txt"),'w') as f:
                for line in self.logbook:
                    f.write(str(line))
                    f.write('\n')
            print("Logs " + filename + " saved successfully")
                          
    def reset(self):
        self.logbook = []
        
    def setVerbosity(self, verbose):
        self.verbose = verbose
        
    def setRealtime(self, realtime):
        self.realtime = realtime
        
        
        
        
        
        
        
        
        