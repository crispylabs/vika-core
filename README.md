# README #

Since it's me and myself for now, I'll use this one for notes. Might find improper lines here and there, pay no mind please.

### Architecture ###

* Python modules
	+ crawler.py
	+ dbmanager.py
	+ agent.py
	+ trainer.py
	+ environment.py
	+ analyst.py
	
* Support files
	+ historical.db

* Folders
	+ networks
	

### Dependencies ###

tbd


### Macro ###

* Agent architecture & training algorithm (HER, TimeSeries)
* UID for versioning and identification of networks upon deployment
* Blind RL for short term trading (MACD based) and Aware RL for long term

##### ARL components #####
* specific crypto's regression for mid/micro trend (granularity 1h/4h) (op:price/EMA/MACD)
* BTC/ETH regression for macro trend analysis (1D might do)
* NLP sentiment analysis on Twitter and news - supervised as a first step, maybe trained on crypto's noise on a later stage


### Micro ###

##### Crawler #####
* make the crawler's output a csv that trainer reads (skip db while possibile)

##### Trainer #####
* debug the scaling and make it parametrized altogether (NO|LOOSE|STRICT)
* code an option to use a timeseries generator as test set
* study the option of using bidirectional LSTMs and Dropout layers
* attempt MACD as only input, then (price,volume,MACD) to see if it's any better